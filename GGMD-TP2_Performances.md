# GGMD : TP2 - Performances et tuning pour le traitement de Masses de Données

L'objectif de ce TP est de vous sensibiliser à l'analyse des performances d'un SGBD et de voir comment améliorer ses performances dans le contexte d'un jeu de données conséquent.

## Introduction

### Jeu de données

Dans ce TP, vous travaillerez sur le jeu de données issu directement de l'INSEE (données brutes sans prétraitement et sans ajout d'identifiant). Vous disposez sur *ggmd_prof2* de la base de données *insee_deces* table *personne_insee*  qui contient les données sur les personnes. 

### Vos VMs

Vous disposez toujours de 3 VMs par groupe. Les IPs de vos VMs sont sur TOMUSS. La clé de connexion est également disponible via tomuss.
Les capacités de vos VMs, en terme de nombre et puissance de CPU et en terme de RAM, sont hétérogènes. L'utilisation de [inxi](https://manpages.ubuntu.com/manpages/bionic/man1/inxi.1.html) vous permettra de déterminer lesdites caractéristiques.

Sur chacune des VMs (grp-XX-small, grp-XX-medium, grp-XX-large), vous disposez d'une installation du SGBD postgreSQL version 15 compatible avec l'extension [CitusData](https://www.citusdata.com/).

Vous pouvez vous authentifier en tant qu'utilisateur 'etum2' (login = password), vous disposez de tous les droits sur la base.

> Pour rappel, la connexion à la base 'insee_deces' par l'utilisateur se fait via la commande :
>
> ```shell
> plsql -h localhost -p 5432 etum2 -d insee_deces
> ```

### A propos des plans d'exécution des requêtes

Pour rappel, vous avez la possibilité d'analyser la performance d'une requête exécutée par un SGBD, grâce à la commande [explain](https://www.postgresql.org/docs/current/sql-explain.html)

Ainsi, la commande :

 ```shell
 explain (analyse, verbose, costs, buffers, timing, summary) select count(*) as nb from personne_insee;
 ```

 vous permet de visualiser le détail de chaque étape du traitement de la requête qui permet de calculer le nombre de tuples dans la table *personne_insee*. Les paramètres _analyse, verbose, costs, buffers, timing, summary_ vous permettent de spécifier ce qui va être calculé et sous quelle forme.

Vous disposez également de l'outil [explain.dalibo](https://explain.dalibo.com/) qui permet une visualisation du plan d'exécution et des statistiques de manière plus graphique.

![image](./img/ex_dalibo.png)


---

> **Travail préparatoire** :
>
> 1. Récupérer les tables 'personne_insee' sur le serveur _ggmd_prof_. Vous serez surement amené à faire du ménage dans votre base. Pour ne pas perdre votre travail précédent, il est recommandé d'archiver la base dans l'état courant _via_ pg\_\dump et un stockage sur le volume de partition /data.
> 2. Quelles différences observez-vous entre la table 'personne' et la table 'personne_insee'?
> 3. Ecrire les requêtes SQL Q1, Q2 et Q3 répondant aux questions précédentes.
> 4. Récupérer dans le fichier de configuration de votre postgresql (/etc/postgresql/15/main/postgresql.conf), les valeurs des paramêtres pouvant impacter les performances du SGBD (shared_buffers, wal_buffers, effective_cache_size, work_mem, maintenance_work_mem,...)



### A - Les requêtes SQL

#### A1 - Focus sur les anomalies

Dans un premier temps, nous allons nous intéresser à des questions en lien avec la qualité des données produites et stockées dans la table *personne_insee*. Il est important de comprendre que des erreurs de saisies au niveau des services des états-civils ainsi que des erreurs dans le processus de collecte de l'INSEE peuvent engendrer des anomalies dans les jeux de données.

Soit le requête Qa suivante : 

```
SELECT nomprenom, datenaiss, lieunaiss, datedeces, lieudeces, count(*) as nb 
		FROM personne_insee 
		GROUP BY nomprenom, datenaiss, lieunaiss, datedeces, lieudeces 
		ORDER BY nb DESC LIMIT 10;
```

> Question 1 : Sans exécuter Qa, expliquer en français de que retourne la requête Qa.

> Question 2 : Définir Qb à partir de Qa pour obtenir le plan d'exécution de celle-ci. Exécuter Qb, puis analyser le résultat retourné.

> Question 3 : Définir le prédicat *is_date* qui prend en argument une chaîne de caractères et qui retourne _true_ si l'argument correspond à une date valide, et false sinon.

> Question 4 : Définir la requête Qc qui retourne le triplet _(nbT, nbDNE, ratio)_ où :
>
> - _nbT_ correspond au nombre total de tuples de la table *personne_insee*, 
> - _nbDNE_ correspond au nombre de tuples avec une Date de Naissance Erronée 
> - _ratio_ correspond à la proportion de tuples ayant une date de naissance erronée.    

> Question 5 : Définir la requête Qd qui retourne le nombre de personnes sans nom de famille.

A noter, que d'autes anomalies ont été identifiées et ont été répertoriées ici : [Memento](https://forge.univ-lyon1.fr/ggmd/ggmd-tps-etudiants/-/blob/main/ressources/memento_donnees_insee.md)


#### A2 - Autres requêtes SQL

Nous allons nous traiter les données pour répondre aux questions suivantes :

Q1. **Pour chaque département de décès, donner le nombre de personnes ayant au moins un doublon de déclaration de décès**. Le résultat sera trié par ordre décroissant du nombre de doublon. Pour cela, vous pouvez vous inspirer de la requête Qa, en sachant que :
- vous aurez à ne garder que les personnes apparaissant plus d'une fois;
- extraire le code département du code INSEE de la commune de décès. Attention aux codes de département sur 3 chiffres des territoires d'outre-mer! (NB: un 'case when' pourrait vous être utile).

Q2. **Pour chaque décénie, donnez le top 10 des noms de famille les plus portés**. Le résultat attendu sera sous la forme : 

````
 année |                                                                                                 classement
-------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  1970 |  1. MARTIN (46717) ; 2. BERNARD (26453) ; 3. PETIT (24870) ; 4. DURAND (24737) ; 5. THOMAS (23512) ; 6. DUBOIS (23279) ; 7. RICHARD (23198) ; 8. ROBERT (22533) ; 9. MOREAU (21943) ; 10. LEFEBVRE (21593)
  1980 |  1. MARTIN (41663) ; 2. BERNARD (23472) ; 3. PETIT (21983) ; 4. DURAND (21909) ; 5. THOMAS (20991) ; 6. DUBOIS (20581) ; 7. RICHARD (20479) ; 8. ROBERT (20048) ; 9. MOREAU (19465) ; 10. LEFEBVRE (19037)
  1990 |  1. MARTIN (33719) ; 2. BERNARD (18937) ; 3. PETIT (17605) ; 4. DURAND (17423) ; 5. THOMAS (16921) ; 6. DUBOIS (16487) ; 7. ROBERT (16366) ; 8. RICHARD (16302) ; 9. MOREAU (15598) ; 10. LEFEBVRE (15060)
  2000 |  1. MARTIN (24598) ; 2. BERNARD (13739) ; 3. PETIT (12738) ; 4. THOMAS (12472) ; 5. DURAND (12456) ; 6. DUBOIS (11959) ; 7. ROBERT (11892) ; 8. RICHARD (11777) ; 9. MOREAU (11307) ; 10. SIMON (10862)
  2010 |  1. MARTIN (14010) ; 2. BERNARD (7777) ; 3. PETIT (7145) ; 4. THOMAS (6993) ; 5. DURAND (6845) ; 6. ROBERT (6707) ; 7. DUBOIS (6636) ; 8. RICHARD (6588) ; 9. MOREAU (6355) ; 10. SIMON (6087)
````


Pour réaliser cette requête :
- vous pourrez dans un premier temps créer une sous-requête SS1 qui génèrera les décénies depuis le 01/01/1970 jusqu'à nos jours. Vous pourrez, pour cela, utiliser la primitive _generate_series()_;
- vous pourrez ensuite créer une seconde sous-requête SS2 qui transformera les données de la table de manière à s'assurer que les tuples considérés ont des dates valides, pour ensuite retourner une extraction du nom de famille et les années de naissance et de décès. Pour l'extraction du nom de famille, vous pourrez utiliser la fonction _split_part_;
- vous pourrez ensuite faire une jointure entre SS1 et SS2 de manière à associer les décénies à chaque personne en fonction de son année de naissance et de son année de décès;
- Ensuite, un regroupement par décénie et nom, vous permettrons de dénombrer les noms;
- Pour finir, l'utilisation d'une fenêtre volante (cf _OVER_, _PARTITION BY_ et du _RANK_) vous permettra d'ordonner les résultats par décénie et l'utilisation de la fonction _STRING_AGG_ vous permettra d'agréger le classement dans un seul attribut.

Pour rappel, l'objectif de ce TP est de traiter des requêtes très couteuses, vous êtes invité à rajouter un _LIMIT 10000_ dans SS2 si vous souhaitez tester votre requête.


Q3. **Quel est la durée de vie moyenne des personnes selon leur département de naissance ?** Pour cela, construire la requête suivante : Pour chaque département (dep et libelle), donner la moyenne des durées de vie des personnes nées dans une commune dudit département. On précisera également le nombre de décès répertoriés pour chaque département et le résultat sera trié par ordre décroissant de durée moyenne de vie.

**Remarque** : Pour la construction de cette requête, il est important de consulter le [memento](https://forge.univ-lyon1.fr/ggmd/ggmd-tps-etudiants/-/blob/main/ressources/memento_donnees_insee.md) répertoriant les anomalies observées dans les données fournies. Vous appliquerez le principe que tout tuple avec une anomalie ne sera pas considéré dans le calcul.

De plus, en considérant les durée de vie comme des intervalles de temps (en postgreSQL : [interval](https://www.postgresql.org/docs/current/datatype-datetime.html) ), vous risquez d'être confronté à un dépassement de valeur maximale dans le calcul de la moyenne. Il vous est recommandé de calculer la moyenne des durées de vie exprimée en jours (et donc par un entier et non un intervalle de temps).

Pour rappel:

- Nombre de jours entre deux dates d1 et d2 : **to_date(d1, _'pattern_date'_) - to_date(d2, _'pattern_date'_ )**
- Transformations d'un nombre de jours _nb_ en un intervalle de temps : **justify_interval( (nb::varchar || ' days')::interval )**

---

## B - Traitement des requêtes sur grp-XX-small

Dans cette section, vous allez utiliser une VM clairement sous-calibrée pour la volumétrie de la base de données hébergée.
L'objectif de cette partie est d'identifier les changement de paramètrage nécessaires pour améliorer les performances de traitement de vos requêtes (même si celles-ci restent insuffisantes).

---

> **Expérimentations GGMD1** :
>
> 1. Identifier les requêtes qui peuvent être traitées sur cette VM. Editer le plan d'exécution desdites requêtes.
> 2. Pour les plans d'exécution qui n'ont pas pu aboutir, expliquer en quelques lignes votre compréhension du problème rencontré.
> 3. Proposer un protocole de résolution pour chaque problème.
> 4. Appliquer vos protocoles. Quel impacts sur le traitement des requêtes?
> 5. Editer un graphique **G1**, de type histogramme empilé, avec en abscisse les requêtes et en ordonnée les temps d'optimisation et temps d'exécution.
> 6. Editer un graphique **G2**, de type histogramme, avec en abscisse les requêtes et en ordonnée le coût de la requête (Pour rappel, le coût se base sur les accès page et les accès tuples)
>
> **Expérimentations GGMD1 - Index** :
>
> 7. Ajouter les index qui vous semblent pertinents.
> 8. Réévaluer les requêtes avec les index. Editer les plan d'exécution. Compléter les graphiques G1 et G2.

> **Expérimentations GGMD1 - configuration** :
>
> 9. Après avoir consulté les recommandations [Tuning your PostgreSQL server](https://wiki.postgresql.org/wiki/Tuning_Your_PostgreSQL_Server), modifier votre configuration (puis redémarrer votre instance).
> 10. Evaluer les requêtes avec votre nouveau paramétrage. Editer les plan d'exécution. Compléter les graphiques G1 et G2.

---

## C - Traitement des requêtes sur grp-XX-medium

Dans cette section, vous allez utiliser une VM plus performante (CPU et RAM) que la précédente. L'objectif de cette partie est d'effectuer un comparatif des performances identifier les limites de la VM utilisée, de voir si un changement de paramètrage est suffisent ou pas pour traiter vos requêtes de manière satisfaisante.

---

> **Expérimentations GGMD2** :
>
> 1. Evaluer les requêtes sans les index. Editer les plan d'exécution. Compléter les graphiques G1 et G2.
>
> **Expérimentations GGMD2 - Index** :
>
> 2. Créer les index.
> 3. Evaluer les requêtes avec les index. Editer les plan d'exécution. Compléter les graphiques G1 et G2.

> **Expérimentations GGMD2 - configuration** :
>
> 4. Evaluer les requêtes avec un nouveau paramétrage (sur le même principe que précédemment). Editer les plan d'exécution. Compléter les graphiques G1 et G2.

---

## D - Traitement des requêtes sur grp-XX-large

Dans cette section, vous allez utiliser une VM encore plus performante (CPU et RAM) que la précédente. L'objectif de cette partie est d'effectuer un comparatif des performances identifier les limites des VM utilisées, de voir si un changement de paramètrage est suffisent ou pas pour traiter vos requêtes de manière satisfaisante.

---

> **Expérimentations GGMD3** :
>
> 1. Evaluer les requêtes sans les index. Editer les plan d'exécution. Compléter les graphiques G1 et G2.
>
> **Expérimentations GGMD3 - Index** :
>
> 2. Créer les index.
> 3. Evaluer les requêtes avec les index. Editer les plan d'exécution. Compléter les graphiques G1 et G2.

---

## E - _Sharding_ via _Citus_ pour le traitement des requêtes

L'objectif de cette partie est d'évaluer l'intérêt de distribuer l'exécution des requêtes sur plusieurs VM.

**ATTENTION**:
Vous allez devoir activer l'extension CITUS sur toutes les instances:

```shell
sudo -i -u postgres psql -c "CREATE EXTENSION citus;"
```

Vous pourrez vous référer à l'[API Citus](https://docs.citusdata.com/en/v12.1/develop/api_udf.html).

**Expérimentations CITUS - Sharding 1** :

> 1.  A partir de la documentatation de [Citus](https://docs.citusdata.com/en/stable/installation/multi_node_debian.html#steps-to-be-executed-on-the-coordinator-node), définir le noeud coordinateur (_coordinator_) et les noeuds de traitements (_worker_)
> 2.  Créer les [tables distribuées](https://docs.citusdata.com/en/stable/get_started/tutorial_multi_tenant.html#distributing-tables-and-loading-data) avec l'attribut de _sharding_ qui vous semble le plus approprié.
> 3.  Evaluer les trois requêtes. Editer les plan d'exécution. Compléter les graphiques G1 et G2.

**Expérimentations CITUS - Sharding 2** :

> 4.  Supprimer les tables distribuées précédentes (après avoir récupérer toutes les informations statistiques nécessaire au compte rendu).
> 5.  Créer les tables distribuées avec un attribut de sharding différent de celui de l'expérimentation précédente.
> 6.  Evaluer les trois requêtes. Editer les plan d'exécution. Compléter les graphiques G1 et G2.

---

## A propos du rendu

Il vous est demandé de déposer un rapport au format PDF dans la case Rendu_TP_perf de tomuss au plus tard le **dimanche 8 octobre 2023**. Ce rapport comprendra au moins :

- un rappel des membres du binôme
- les copies d'écran des plans d'exécutions (de préférence version visuelle avec explain.dalibo) que vous avez effectué, en précisant à chaque fois le contexte du plan (requête / machine / mode de traitement ). Vous préciserez également les statistiques.
- deux graphiques de synthèse des performances permettant de visualiser pour chaque requête et en ordonnées le critère de performance évalué (temps, coût) comme dans l'exemple suivant:

![image](./img/statistiques.png)

Si cela vous semble nécessaire, il sera possible également d'ajouter un court retour d'expérience relatant les problèmes rencontrés dans votre rapport.
