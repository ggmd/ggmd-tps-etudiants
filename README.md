# GGMD :  TPs 2023

## Partie Base de Données Répartie

- [TP1] [Déploiement d'une base de données répartie](./GGMD-TP1_Fragmentation.md)
- [TP2] [Performances et tuning pour le traitement de Masses de Données](./GGMD-TP2_Performances.md)
- [TP3] [TP Big Data sur données astronomiques : Apache Spark](https://forge.univ-lyon1.fr/ggmd/tp-spark-2023-etudiants)
- [TP4] [Traitement de flux via Spark Streaming](./GGMD_TP4_SparkStreaming.md)
- [TP5] [Traitement de flux via Apache STORM](https://forge.univ-lyon1.fr/ggmd/tp-storm-2023/) 


