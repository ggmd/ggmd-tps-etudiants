## Traitement de flux *via* Spark Streaming


L'objectif de ce TP est de vous familiariser avec l'API de Spark Streaming.



### Contexte
Pour ce TP, nous considérons une pseudo piste d'athlétisme.
Cette piste est composée de 35 couloirs fragmentés en cellule. Chaque
couloir est composée de 254 cellules numérotées. Cette piste est le
terrain d'une course de tortue. Une tortue peutau plus avancer de deux cellules entre chaque top.
L'évolution de la course est décrite par un flux de données dont le
schéma est (id, top, position, nbAvant, nbTour) avec id un
entier correspondant au dossard de la tortue qui l'identifie, top un
entier qui indique le numéro d'observation des tortues sur la piste,
position un entier qui correspond à la cellule courante où se trouve
la tortue (Attention, la position ne permet pas de déterminer le
classement de la tortue, car la piste est circulaire et qu'une tortue
peut avoir au moins un tour d'avance), nbDevant un entier qui indique
le nombre de tortues se trouvant devant la tortue dans le classement et nbTour indique le nombre de tour de piste effectué par la tortue.

![image](./img/tortues.png)




### Les flux 
Vous disposez de 2 flux hébergés sur la VM 192.168.76.188. Le premier est accessible sur le port 9001 est retourne les observations d'avancement des tortues sur la piste. Le second flux est accessible sur port 9002 et il diffuse des informations sur l'affectation des tortues à des équipes. Le schéma de ce flux est (tortoise, team) où *tortoise* correspond à l'id de la tortue et *team* au nom de l'équipe à laquelle la tortue est rattachée. 


### Réalisation du TP
Pou réaliser ce TP, vous devrez utiliserez le cluster du TP précédent avec comme point d'entrée la VM dont l'IP est spécifiée dans la colonne *spark_vm*.

Vous disposez du script [tortoiseListener.py](https://forge.univ-lyon1.fr/ggmd/ggmd-tps/-/blob/main/ressources/tortoiseListener.py) que vous pourrez tester en exécutant la commande :

`spark-submit /home/p123456/tortoiseListener.py > res_tortoiseListener.log`

La suite du TP consiste à développer de nouvelles fonctionnalités en python. Pour cel, vous pourrez vous appuyer sur la documentation [Spark Streaming](https://spark.apache.org/docs/latest/streaming-programming-guide.html).



### Fonctionnalités attendues
1. filtrer que les tuples qui concerne une tortue. Pour cela, vous considérez comme votre tortue, celle dont l'id correspond à votre numéro de groupe (colonne Num_equipe_tortues dans Tomuss)
2. convertir les données du flux en objet JSON respectant le schéma (id, top, position, nbAvant, nbTotal)
3. connaitre la distance parcourue par votre tortue. Le résultat sera exprimé en nombre de cellules. Vous veillerez à tenir compte des tour complet.
4. connaitre le podium individuel et temporaire (les tortues qui sont sur les 3 premières marches du podium; attention, il peut y avoir des ex aequo) tous les 30 secondes. Le résultat sera un objet JSON de la forme {'1er': [id1, id2], '2eme':[id3], '3eme:[id4, id5]}.
5. avoir les informations sur les tortues et leur rattachement à une équipe. On procédera par jointure des flux.
6. connaitre le podium par équipe et temporaire (les tortues qui sont sur les 3 premières marches du podium; attention, il peut y avoir des ex aequo) tous les 30 secondes. Le résultat sera un objet JSON de la forme {'1er': [nomEquipe1], '2eme':[nomEquipe2,nomEquipe3], '3eme:[nomEquipe4]}. 
7. la vitesse moyenne de votre tortue sur une fenêtre de 30 secondes toutes les minutes. Pour cela, il sera nécessaire de connaitre le nombre de *top* représenté dans la fenêtre et la distance parcourue. 
8. l'évolution de la vitesse de votre tortue de fenêtre de 30 secondes en fenêtre de 30 secondes. Le résultat sera de la forme {'fenetre':i , 'evolution': 'Augmentation'} dans le cas où la vitesse moyenne calculée dans la fenêtre *i* est supérieure à la vitesse observée dans la fenêtre *i-1*. Sinon l'attribut évolution sera soit 'Diminution', soit 'Egalité'. A noter que pour ce type de requête, il est nécessaire de conserver l'état précédent.


