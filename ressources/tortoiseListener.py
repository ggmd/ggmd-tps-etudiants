# -*- coding: utf-8 -*-
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
import sys
import json
import pprint
 	
compte = "Nom_Compte"


if __name__ == "__main__":

	# variables
	appName = "TortoiseListener_GrpXX"
	myTortoiseID = 0
	sourceIP = "192.168.76.188"
	sourcePort = 9001
	window = 10    #fenêtre de 10 secondes

	# creation des contextes
	sc = SparkContext("local[*]", appName)
	ssc = StreamingContext(sc, window)

	# récupération des données
	dstream = ssc.socketTextStream(sourceIP, sourcePort)
	
	# transformations des données
	tortoises = dstream.map(lambda l : list(map(lambda c: c.strip(), l.split(";"))))
	
	# affichage des données
	tortoises.pprint()

    	# lancement
	ssc.start()             
	ssc.awaitTermination()   
	

