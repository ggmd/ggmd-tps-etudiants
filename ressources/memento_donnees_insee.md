# Spécificités du jeu de données INSEE sur les décès


### Schéma :

personne (nomprenom varchar(100), sexe char(1), datenaiss varchar(10), lieunaiss varchar(10) ,commnaiss varchar(80), paysnaiss varchar(50), datedeces varchar(10), lieudeces varchar(10), actedeces varchar(50) )

### Informations :

- période officielle de couverture des décès : 1970 - 2019
- intervalle d'années de décès : 1882 - 2019
- Nombre de tuples : 24 916 669 tuples

- Nombre de personnes dont le nom et prenom dépasse les 80 caractères (pas de / à la fin des prénoms) : 66 

- Nombre de personnes mortes avant d'être nées : 83
- Nombre de personnes avec une date de naissance invalide (chaîne de caractères vide) : 0
- Nombre de personnes nées un 29 février d'une année non bissextile : 21
- Nombre de personnes nées un 30 février : 2
- Nombre de personnes nées le 31 d'un mois ayant au plus 30 jours : 12
- Nombre de personnes avec uniquement le jour de naissance inconnu ( jour=00) : 16910
- Nombre de personnes avec uniquement le mois de naissance inconnu (mois=00) : 6
- Nombre de personnes avec le mois et le jour de naissance inconnus (mois=00; jour=00) : 71 699
- Nombre de personnes avec une date de naissance inconnue (année=0000; mois=00; jour=00) : 549


- Nombre de personnes avec une date de décès invalide (chaîne de caractères vide) : 2
- Nombre de personnes décédées un 29 février d'une année non bissextile : 0
- Nombre de personnes avec une date de décès sur moins de chiffres : 7
- Nombre de personnes avec uniquement le jour de décès inconnu ( jour=00) : 405
- Nombre de personnes avec uniquement le mois de décès inconnu (mois=00) : 11
- Nombre de personnes avec le mois et le jour de décès inconnus (mois=00; jour=00) : 224
- Nombre de personnes avec une date de décès inconnue (année=0000; mois=00; jour=00) : 0
