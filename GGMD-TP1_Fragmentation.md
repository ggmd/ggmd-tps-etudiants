# GGMD : TP 1 - Déploiement d'une base de données répartie

_prérequis_: Avoir réaliser la fiche d'exercices applicatifs 'BDR : Fragmentation d'une base de données'

L'objectif de ce TP est d'implémenter les fragments et les vues unifiées définis algébriquement durant les exercices applicatifs. Nous nous intéresserons également à la réplication logique des mairies.

## Rappel à propos des données

Nous considérons la base de données _decesFrance_ issue des données publiées par l'INSEE et DATAGOUV.FR et légèrement transformées dans le cadre de cette série d'exercices. Le schéma des données est le suivant:

![image](./img/schema.png)

Pour rappel, nous nous intéressons aux requêtes suivantes :

- Q1. Donner les personnes (idp, nom, prenom) nées dans la région 'Hauts-de-France' et décédées dans la région 'Occitanie'.

- Q2. Donner les adresses (postales et mail) des mairies des communes de naissance des personnes décédées dans le département 63 durant le mois d'avril 2018.

- Q3. Donner le nombre de personnes dont le code de la commune de naissance n'existe plus en 2022.

> TODO :

Avant de commencer la fragmentation, vous pouvez vous connecter à la base _insee_ sur _ggmd_prof_ pour tester vos requêtes.

> Résultats attendus : Q1 retourne 54 357 tuples; Q2 retourne 54 357 tuples; Q3 retourne la valeur 3 127 570 ou 3 127 559 selon les conditions imposées.

## A - Définition des connexions inter-bases

La base de données centralisée que vous devez fragmenter se nomme _insee_ et elle est hébergé sur la VM _ggmd_prof_.
Pour vous connecter à la base, les informations sont sur TOMUSS.
Chaque binôme dispose de 3 VMs _ggmd1_, _ggmd2_ et _ggmd3_ dont les IPs vous sont attribuées sur TOMUSS.

> TODO :

Sur chacune de vos VM :

1.  Définir le 'wrapper' qui vous permettra de vous connecter à la base _insee_ sur _ggmd_prof_.
2.  Définir l'association utilisateur local/ utilisateur distant qui vous donnera le droit d'accès à la base _insee_.
3.  Définir les 'foreign tables' qui vous permettront d'accéder à la base _insee_ depuis vos VM

## B - Fragmentation des données

On souhaite implémenter la fragmentation de la base de données. On supose ici que _ggmd1_ correspond au serveur du site de **Paris**, _ggmd2_ à celui de **Lyon** et _ggmd3_ a celui de **Marseille**.

> TODO :

A partir des définitions algébriques dont vous disposez, créer et peupler les tables nécessaires à l'implantation de vos fragments.

**IMPORTANT** : la base de données _insee_ sur _ggmd_prof_ sera à termes supprimées. Il sera alors important que vous n'ayez pas perdus de données.

## C - Mise en place de la réplication logique des mairies

On souhaite à présent gérer les mairies et mettre en place leur réplication à partir du site de référence _ggmd3_. Cette réplication repose sur la notion de publication et de subscription (publish/subscribe)
.

> TODO :

1.  Importer la table _mairie_ sur _ggmd3_
2.  Créer une 'publication' des mairies locales
3.  Créer les subscriptions nécessaires sur _ggmd1_ et _ggmd2_ pour disposer d'une réplication logique des mairies sur ces sites.
4.  Depuis _ggmd3_ modifier la date de mise à jour associée à la mairie de Villeurbanne et tester si la synchronisation s'est bien opérée sur tous les sites.

## D - Interrogation des fragments

On souhaite exécuter les requêtes définies dans l'exercice 1 de la fiche d'exercices applicatifs.

> TODO :

1.  Réécrire les requêtes Q1, Q2 et Q3 à partir des fragments dont vous disposez et de manière à exécuter les requêtes depuis _ggmd1_
2.  Tester vos requêtes

## E - Création des vues globales

On souhaite à présent offrir à l'utilisateur la possibilité d'exécuter les requêtes Q1, Q2 et Q3 telles qu'elles étaient définies dans l'exercice 1 de la fiche d'exercices applicatifs, autrement dit, en fonction du schéma global.

> TODO :

1.  A partir des expressions algébriques de l'exercice 3, définir les vues globales permettant d'accèder à l'ensemble des données réparties sur vos 3 VM et ce, selon le schéma initial.
2.  Tester Q1, Q2 et Q3

## BONUS - Gestion des contraintes d'intégrité globale

Les contraintes d'intégrité de clé primaire ou de clé étrangère qui peuvent s'appliquer sur chaque fragment ne permette pas de garantir l'intégrité de manière globale

> TODO

Définir les triggers nécessaires pour garantir l'intégrité globale de votre base de données répartie.
